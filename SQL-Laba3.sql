
--- #1
SELECT SUM(Price) from Service WHERE ID IN
(	
	SELECT ServiceID FROM ServiceFlat WHERE FlatID = 
	(
		SELECT ID FROM Flat WHERE Number = 50 AND HouseID = 
		(
			Select ID FROM House WHERE Number = 2 AND StreetID = 
			(
				Select ID FROM Street WHERE Name = 'Садовая'
			)
		)
	)
)

--- #2

SELECT
(
	SELECT Price From Service WHERE Name = 'доставка мусора'
)
*
(
	SELECT COUNT( * ) FROM  Tariff WHERE DATEDIFF( CURDATE( ) , Month ) >= 0
)
*
(
	SELECT COUNT( * ) FROM ServiceFlat WHERE ServiceID = (SELECT ID From Service WHERE Name = 'доставка мусора') AND FlatID IN
	(
		SELECT ID FROM Flat WHERE OwnerID = 
		(
			SELECT ID FROM Owner WHERE Name = 'Константинов Роман Иванович'
		)
	)
)
-
(
	SELECT SUM(Amount) FROM Payment
	WHERE ServiceID = 
	(
		SELECT ID FROM Service WHERE Name = 'доставка мусора'
	)
	AND ForMonth IN
	(
		SELECT Month FROM  Tariff WHERE DATEDIFF( CURDATE( ) , Month ) >= 0
	)
	AND FlatID IN
	(
		SELECT ID FROM Flat WHERE OwnerID = 
		(
			SELECT ID FROM Owner WHERE Name = 'Константинов Роман Иванович'
		)
	)
)


--- #3
SELECT SUM(Amount) FROM Payment WHERE FlatID IN 
(
	SELECT ID FROM Flat WHERE HouseID IN 
	(
		SELECT ID FROM House WHERE StreetID = 
		(
			SELECT ID FROM Street WHERE Name = 'Первомайская'
		)
	)
)


--- #4
SET SQL_BIG_SELECTS = 1;
SELECT Name From Owner WHERE ID IN
(
	SELECT OwnerID FROM Flat Where ID IN 
	(
		SELECT ts.FlatID
		FROM (SELECT * FROM Tariff,ServiceFlat) ts
		LEFT OUTER JOIN Payment
		ON ts.Month = Payment.ForMonth
		AND ts.ServiceID = Payment.ServiceID
		AND ts.FlatID = Payment.FlatID
		JOIN Service
		ON ts.ServiceID = Service.ID
		WHERE DATEDIFF(CURDATE( ),ts.Month) > 365
		AND Service.Price > COALESCE(Payment.Amount,0)
	)
);
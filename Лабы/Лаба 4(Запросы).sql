
--- #1 Определить сумму месячной оплаты для квартиры №50 в доме 2 по Садовой улице.
SELECT SUM(Price) from Service WHERE ID IN
(	
	SELECT ServiceID FROM ServiceFlat WHERE FlatID = 
	(
		SELECT ID FROM Flat WHERE Number = 50 AND HouseID = 
		(
			Select ID FROM House WHERE Number = 2 AND StreetID = 
			(
				Select ID FROM Street WHERE Name = 'Садовая'
			)
		)
	)
)

--- #2 Определить задолженность по оплате 'доставка мусора' Константинова Романа Ивановича.

SELECT
(
	SELECT Price From Service WHERE Name = 'доставка мусора'
)
*
(
	SELECT COUNT( * ) FROM  Month WHERE DATEDIFF( CURDATE( ) , Month ) >= 0
)
*
(
	SELECT COUNT( * ) FROM ServiceFlat WHERE ServiceID = (SELECT ID From Service WHERE Name = 'доставка мусора') AND FlatID IN
	(
		SELECT ID FROM Flat WHERE OwnerID = 
		(
			SELECT ID FROM Owner WHERE Name = 'Константинов Роман Иванович'
		)
	)
)
-
(
	SELECT SUM(Amount) FROM Payment
	WHERE ServiceID = 
	(
		SELECT ID FROM Service WHERE Name = 'доставка мусора'
	)
	AND ForMonth IN
	(
		SELECT Month FROM  Month WHERE DATEDIFF( CURDATE( ) , Month ) >= 0
	)
	AND FlatID IN
	(
		SELECT ID FROM Flat WHERE OwnerID = 
		(
			SELECT ID FROM Owner WHERE Name = 'Константинов Роман Иванович'
		)
	)
)


--- #3 Определить общюю сумму выплат для квартир по Первомайской улице.

SELECT SUM(Amount) FROM Payment WHERE FlatID IN 
(
	SELECT ID FROM Flat WHERE HouseID IN 
	(
		SELECT ID FROM House WHERE StreetID = 
		(
			SELECT ID FROM Street WHERE Name = 'Первомайская'
		)
	)
)


--- #4 Выбрать список владельцев, которые имеют задолженности более года.
SET SQL_BIG_SELECTS = 1;
SELECT Name From Owner WHERE ID IN
(
	SELECT OwnerID FROM Flat Where ID IN 
	(
		SELECT ts.FlatID
		FROM (SELECT * FROM Month,ServiceFlat) ts
		LEFT OUTER JOIN Payment
		ON ts.Month = Payment.ForMonth
		AND ts.ServiceID = Payment.ServiceID
		AND ts.FlatID = Payment.FlatID
		JOIN Service
		ON ts.ServiceID = Service.ID
		WHERE DATEDIFF(CURDATE( ),ts.Month) > 365
		AND Service.Price > COALESCE(Payment.Amount,0)
	)
);


--- свой запрос на update

UPDATE Service SET Price = 700 WHERE ID = 2

--- свой запрос на update, нарушение целостности 

UPDATE Street SET ID = 2 WHERE Name = "Садовая"

--- свой запрос на delete, вызывает каскадное удаление квартир и записей из ServiceFlat

DELETE FROM Owner WHERE Name = 'Константинов Роман Иванович'
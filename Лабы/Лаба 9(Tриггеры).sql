---оплата не может производится на срок более, чем 12 месяцев вперед

--- вариант 1
CREATE TRIGGER ForwardDate1
	ON Payment AFTER INSERT AS
    IF EXISTS (
    	SELECT * FROM Payment
    	WHERE MONTH(DATEDIFF( CURDATE (), (SELECT MAX(ForMonth) FROM INSERTED))) < -12
    )
    BEGIN
	ROLLBACK
	END
	
---вариант 2
CREATE TRIGGER ForwardDate2
ON Payment
INSTEAD OF INSERT
	INSERT INTO Payment
    SELECT * FROM INSERTED
    WHERE MONTH(DATEDIFF( CURDATE (), (SELECT MAX(ForMonth) FROM INSERTED))) < -12
    
    
---Зарубают оба
INSERT INTO Payment (ForMonth,Amount,FlatID,ServiceID) VALUES ("01-01-2026",50,450,5) 

---1 зарубает, а 2 фильтрует и вставляет только вторую оплату
INSERT INTO Payment (ForMonth,Amount,FlatID,ServiceID) VALUES ("01-01-2026",50,450,5),("01-01-2016",50,450,5) 

--- пропускают оба
INSERT INTO Payment (ForMonth,Amount,FlatID,ServiceID) VALUES ("01-01-2015",50,450,5),("01-01-2016",50,450,5) 
